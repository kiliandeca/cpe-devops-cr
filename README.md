# Docker TP 01

## Database

pg.Dockerfile
```Dockerfile
FROM postgres:12.1-alpine

ENV POSTGRES_DB=db \
    POSTGRES_USER=usr \
    POSTGRES_PASSWORD=pwd
```
> We use alpine image because it's lightweight

Build:
```
docker build -f pg.Dockerfile . -t my-pg
```
> -f: we to specify file name since we use multiple Dockerfile in the TP

> -t: the local image name

Run:
```
docker run --rm -p 5432:5432 my-pg
```
> -p: we bind local port 5432 to container port 5432

Run with env good practice:
```
docker run --rm \
    -e POSTGRES_DB=db \
    -e POSTGRES_USER=usr \
    -e POSTGRES_PASSWORD=pwd \
    -p 5432:5432 postgres:12.1-alpine
```
> -e: we set env var when creating the container
> 
***
## Init database

db/01-CreateScheme.sql
```sql
CREATE TABLE public.departments (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20) NOT NULL
);

CREATE TABLE public.students(
    id SERIAL PRIMARY KEY,
    department_id INT NOT NULL REFERENCES departments (id),
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL
);
```


db/02-InsertData.sql
```sql
INSERT INTO departments (name) VALUES ('IRC');
INSERT INTO departments (name) VALUES ('ETI');
INSERT INTO departments (name) VALUES ('CGP');
INSERT INTO students (department_id, first_name, last_name) VALUES (1, 'Eli', 'Copter');
INSERT INTO students (department_id, first_name, last_name) VALUES (2, 'Emma', 'Carena');
INSERT INTO students (department_id, first_name, last_name) VALUES (2, 'Jack', 'Uzzi');
INSERT INTO students (department_id, first_name, last_name) VALUES (3, 'Aude', 'Javel');
```

Run with initdb scripts:
```bash
docker run --rm \
    -e POSTGRES_DB=db \
    -e POSTGRES_USER=usr \
    -e POSTGRES_PASSWORD=pwd \
    -v $PWD/db:/docker-entrypoint-initdb.d:ro \
    -v /data/pg:/var/lib/postgresql/data \
    -p 5432:5432 postgres:12.1-alpine
```
> -v: we bind volume db containing init script with :ro for readonly, we bind the folder /data to have persistence when recreating the container

## Backend API

### Java

```Dockerfile
FROM openjdk:11-slim as builder
# Build Main.java
COPY ./java /usr/src/
WORKDIR /usr/src/

RUN javac Main.java

FROM openjdk:11-jre-slim
# Copy resource from previous stage
COPY --from=builder /usr/src/Main.class .

ENTRYPOINT ["java", "Main"]
```
> To reduce the final image size we use a multistage build, the first image have the tools needed to compile the source and the runner immage only what is necessery to run the compiled application

### Hello World

Dockerfile:

```Dockerfile
# Build
FROM maven:3.6.3-jdk-11-slim AS myapp-build
ENV MYAPP_HOME /opt/myapp
WORKDIR $MYAPP_HOME
COPY pom.xml .
COPY src ./src
RUN mvn package -DskipTests

# Run
FROM openjdk:11-jre-slim
ENV MYAPP_HOME /opt/myapp
WORKDIR $MYAPP_HOME
COPY --from=myapp-build $MYAPP_HOME/target/*.jar $MYAPP_HOME/myapp.jar
ENTRYPOINT java -jar myapp.jar
```

Build:

```
docker build -f api.Dockerfile . -t api-java
```

Run:
```
docker run --rm -p 8080:8080 api-java
```

### API

Build:
```
docker build . -t api-java-2
```

Create a bridge network:
```
docker network create --driver bridge super-app-net
```
> We create a bridge network named super-app-net

```docker
docker run --rm \
    -e POSTGRES_DB=db \
    -e POSTGRES_USER=usr \
    -e POSTGRES_PASSWORD=pwd \
    -v $PWD/db:/docker-entrypoint-initdb.d:ro \
    -v /data/pg:/var/lib/postgresql/data \
    --network super-app-net \
    --name db \
    postgres:12.1-alpine
```
> We run the database without exposing the port outside of the bridge network

Run:
```bash
docker run --rm --network=super-app-net \
-p 8080:8080 --name back \
api-java-2
```
> We run the java app and link the network

## HTTP Server

```bash
docker cp youthful_poincare:/usr/local/apache2/conf/httpd.conf .
```
> We use docker cp instead of exec to retrieve the httpd.conf file to the current directory

On active les extendions *mod_proxy.so* et *mod_proxy_hhtp.so*

Run:
```bash
docker run --rm --network=super-app-net \
-p 80:80 --name front \
-v $PWD/httpd.conf:/usr/local/apache2/conf/httpd.conf \
httpd:2-alpine
```

> note: We remove the back-end port mapping


## Docker Compose

Docker compose is a simpler way of defining docker containers, networks, ...

With a simple docker-compose up we can launch or update a complexe docker configuration

```yaml
version: '3.5'
services:
    db:
        image: postgres:12.1-alpine
        environment: 
            POSTGRES_DB: db
            POSTGRES_USER: usr
            POSTGRES_PASSWORD: $pwd
        volumes: 
            - ./db:/docker-entrypoint-initdb.d:ro
            - /data/pg:/var/lib/postgresql/data
        networks:
            - db-net
    back:
        build: simple-api-2
        networks:
            - db-net
            - proxy-net
        depends_on:
            - db
    httpd:
        image: httpd:2-alpine
        ports:
            - 80:80
        volumes: 
            - ./httpd.conf:/usr/local/apache2/conf/httpd.conf:ro
        networks:
            - proxy-net
        depends_on:
            - back
networks:
    db-net:
    proxy-net:
```

We use host env var $pwd to set the container env var for POSTGRES_PASSWORD

We bind somes volumes for:
    - db init scripts
    - db persistence
    - httpd config

We use 2 networks to isolate container, this way the httpd container can't communicate with the database container since it doesn't need to

# Travis CI TP 02

## Concepts et syntaxe
En ajoutant un fichier **.travis.yml** à la racine de notre projet Travis va être activé, il est capable de detecter que c'est un projet Java et faire les actions par defauts pour un projet Maven.

On peut spécifier le language:

```yaml
language: java
```

On peut ajouter un dossier au cache entre plusieurs jobs ce qui est utile avec les dependences Mavence et qui évite de les retélécharger à chaque job


```yaml
cache:
    directories:
        - $HOME/.m2
```


Ajouter un script:
```yaml
script:
    - mvn clean verify
```

pipeline multi jobs:
```yaml
jobs:
    include:
        - stage: test
          script:
            - mvn clean verify
        - stage: build
          script:
            - mvn package -DskipTests
```

Jobs paralleles
```yaml
jobs:
    include:
        - stage: test
          script:
            - echo job 1
          script:
            - echo job 2
          script:
            - echo job 3
```

Variables d'environements:

Il est possible de définir des variables d'environement qui seront ajouter lors de l'execution des jobs. Depuis l'interface ou depuis le client ruby travis. 
>Ces variables sont chiffrés et c'est impossible de les consulter, pour les modifier il faut la recréer

Utilisez des addons:
```yaml
addons:
    sonarcloud:
        organization: "kiliandeca"
        token:
            secure: "$SONAR_TOKEN"
```

Utilisez des services:

```yaml
services:
  - docker
script:
    # Build Java
    - mvn package -DskipTests

    #Build Docker
    - docker build -t kiliandeca/sample-cpe:api sample-application-http-api-server

```

## Resultat TP

```yaml
language: java

cache:
    directories:
        - $HOME/.m2

jobs:
    include:
        - stage: test
          addons:
            sonarcloud:
                organization: "kiliandeca"
                token:
                    secure: "$SONAR_TOKEN"
          script:
            - mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install sonar:sonar -Dsonar.projectKey=Kiliandeca_sample-application-students

        - stage: build
          services:
            - docker
          script:
            # Build Java
            - mvn package -DskipTests

            #Build Docker
            - docker build -t kiliandeca/sample-cpe:api sample-application-http-api-server
            - docker build -t kiliandeca/sample-cpe:db sample-application-db-changelog-job

            # Publish
            - docker login -u kiliandeca -p $DOCKER_HUB_SECRET
            - docker push kiliandeca/sample-cpe:api
            - docker push kiliandeca/sample-cpe:db
```

# Ansible TP 03

## Basic ping playbook
```yaml
- hosts: all
  gather_facts: false
  become: yes

  tasks:
    - name: Test connection
      ping:
```

## Roles

Creation d'un role:
```yaml
ansible-galaxy init roles/monrole
```

## Ouverture des ports sur la machine

```yaml
- name: Open port 80
  firewalld:
    port: 80/tcp
    permanent: yes
    state: enabled
 
- name: Open port 443
  firewalld:
    port: 443/tcp
    permanent: yes
    state: enabled

- name: Open port 22
  firewalld:
    port: 22/tcp
    permanent: yes
    state: enabled
```
> On utilise le module firewalld

## Lancement d'un container

```yaml
- name: Start container
  docker_container:
    name: api-http
    image: kiliandeca/sample-cpe:api
```

# Extra ELK

Instalation Elastic:

```bash
ansible-galaxy install elastic.elasticsearch,7.5.2
```

Ansible-galaxy est un repo communautaire de roles ansible. Il y a beaucoup de roles communs qui sont déjà fait que l'on peut reprendre ensuite dans nos playbook.

```yaml
- name: Simple Example
  hosts: all
  roles:
    - role: elastic.elasticsearch
  vars:
    es_version: 7.5.2
```
